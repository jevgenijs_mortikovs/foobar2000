#pragma once

// fb2k mobile compatibility
#include <functional>
namespace fb2k {
	void inWorkerThread(std::function<void()> f);
}
